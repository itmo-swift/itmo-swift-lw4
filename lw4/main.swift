//
//  main.swift
//  lw4
//
//  Created by Daniil Lushnikov on 26.09.2021.
//  Comparing of pair a+b and c

import Foundation

let a = Double(readLine() ?? "") ?? 0
let b = Double(readLine() ?? "") ?? 0
let c = Double(readLine() ?? "") ?? 0
let sum = Decimal(a) + Decimal(b)
let clarifiedC = Decimal(c)
compareTwoNumbers(first: sum, second:clarifiedC)

func compareTwoNumbers(first: Decimal, second:Decimal) -> Void {
    switch sum {
    case _ where sum < clarifiedC:
        print("<")
    case _ where sum == clarifiedC:
        print("=")
    case _ where sum > clarifiedC:
        print(">")
    default:
        print("This is impossible")
    }
}
